all: compile

compile:
	rebar3 compile

clean:
	rebar3 clean
distclean:
	rm -f TEST-*.xml
	rm -rf _build rebar.lock

list-deps:
	rebar3 deps
eunit:
	rebar3 eunit
xref:
	rebar3 xref
dialyzer:
	rebar3 dialyzer

cc: clean compile

ccx: clean compile xref eunit
