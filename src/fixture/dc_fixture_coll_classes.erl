%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Controlled classes descriptions (for dms collection 'classes')

-module(dc_fixture_coll_classes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([domains/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% CLASS "domains"
%% return fixtured entity of controlled class'
%% -------------------------------------
domains(_Domain) ->
    #{
        <<"id">> => ?BU:to_guid(<<"5951512f-0179-d0aa-e797-7cd30a921f58">>),
        <<"classname">> => ?DomainsCN,
        <<"name">> => <<"Domains">>,
        <<"description">> => <<"General platform collection. Domains for data isolation.">>,
        <<"storage_mode">> => ?MasterStorageMode,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => <<"dc:validator">>,
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => false,
            <<"max_limit">> => 9999,
            <<"storage_instance">> => ?MasterStorageInstance,
            <<"partition_property">> => <<>>,
            <<"lookup_properties">> => [<<"name">>],
            <<"store_changehistory_mode">> => <<"sync">>
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"name">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"master_dbstrings">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => false,
                <<"default">> => [],
                <<"multi">> => true
            }
        ]}.

%% ====================================================================
%% Internal functions
%% ====================================================================