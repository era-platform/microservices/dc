%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Validates 'domain' entity on any modify operation.

-module(dc_validator_coll_domains).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([validate/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(DomainDbName(DomainName), ?BU:strbin("era_master__~ts",[DomainName])).

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Validate storage entity on any modify operation
%% --------------------------------------
-spec validate(Domain::binary() | atom(),
               Operation :: create | replace | update | delete,
               EntityInfo::{Id::binary(),E0::map(),E1::map()} | undefined,
               ModifierInfo::{Type::atom(),Id::binary()} | undefined) ->
    {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
validate(Domain,Operation,{_Id,_E0,E1}=_EntityInfo,_ModifierInfo) ->
    case Operation of
        'delete' -> {ok,undefined};
        _ ->
            try
                case check_entity(Domain,Operation,E1) of
                    {error,_}=Err -> Err;
                    {ok,EntityV} ->
                        case validate_all_domains(Domain,EntityV) of
                            {error,_}=Err -> Err;
                            ok ->
                                case ensure_master_db(Domain,EntityV) of
                                    {error,_}=Err -> Err;
                                    {ok,EntityV1} -> {ok,EntityV1}
                                end end end
            catch E:R:ST ->
                ?LOG('$error',"~ts. '~ts' validation subdomain '~ts' crash: ~n\t~120tp~n\tStack: ~120tp",[?APP, Domain, maps:get(<<"name">>,E1), {E,R},ST]),
                {error,R}
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Check domain individual properties
%% -------------------------------------
check_entity(Domain,Operation,Item) ->
    check_1(Domain,Operation,Item).

%% ----------------------
%% domain name (one-level subdomain of parent)
check_1(Domain,Operation,Item) -> check_1a(Domain,Operation,Item).

%% domain name should be one-level subdomain of parent
check_1a('root'=Domain,Operation,Item) ->
    check_1b(Domain,Operation,Item);
check_1a(Domain,Operation,Item) when is_binary(Domain) ->
    Name = maps:get(<<"name">>,Item),
    case ?BU:extract_parent_domain(Name) of
        Domain -> check_1b(Domain,Operation,Item);
        _ -> {error, {invalid_params, <<"field=name|Invalid 'name'. Must be one-level subdomain">>}}
    end.
%% domain name (not "root")
check_1b(Domain,Operation,Item) ->
    case ?BU:to_list(maps:get(<<"name">>,Item)) of
        <<"root">> -> {error, {invalid_params, <<"field=name|Invalid 'name'. You cannot set reserved domain's name 'root'.">>}};
        _ -> check_1c(Domain,Operation,Item)
    end.
%% domain name (valid symbols)
check_1c(Domain,Operation,Item) ->
    Name = ?BU:to_list(maps:get(<<"name">>,Item)),
    F = fun(X) when X==$.; X==$_; X==$- -> true;
           (X) when X>=$a, X=<$z -> true;
           (X) when X>=$0, X=<$9 -> true;
           (_) -> false
        end,
    case lists:all(F, Name) of
        true -> check_2(Domain,Operation,Item);
        false -> {error, {invalid_params, <<"field=name|Invalid 'name'. Invalid symbols. Could contain [a-z0-9_-.]">>}}
    end.

%% ----------------------
%% check master storages.
check_2(Domain,Operation,Item) -> check_2a(Domain,Operation,Item).
%% for required keys. Fill by parent domain's master storages if empty list

check_2a(Domain,Operation,Item) ->
    case maps:get(<<"master_dbstrings">>,Item,[]) of
        [] ->
            ReadOpts = #{<<"filter">> => [<<"&&">>,[<<"==">>,[<<"property">>,<<"instance">>],?MasterStorageInstance],[<<"==">>,[<<"property">>,<<"type">>],?MasterStorageType]]},
            case ?DMS_CACHE:read_cache(Domain,?StoragesCN,ReadOpts,auto) of
                {error,_}=Err ->
                    ?LOG('$error',"~ts. '~ts' validation subdomain '~ts' error reading master storages: ~120tp",[?APP, Domain, Err]),
                    Err;
                {ok,[],_} ->
                    ?LOG('$error',"~ts. '~ts' validation subdomain '~ts' error: no master storages found",[?APP, Domain]),
                    {error,{internal_error,<<"Master storages not found in domain">>}};
                {ok,StorageItems,_} ->
                    DbStrings = lists:map(fun(StorageItem) ->
                                                Params = maps:get(<<"params">>, StorageItem),
                                                build_connstring([{K,maps:get(K,Params)} || K <- connstring_keys()])
                                          end, StorageItems),
                    check_2b(Domain,Operation,Item#{<<"master_dbstrings">> => DbStrings})
            end;
        [_|_]=DbStrings ->
            case lists:any(fun(DbParam) -> lists:member(undefined,maps:values(DbParam)) end, get_db_params(DbStrings)) of
                true ->
                    {error,{invalid_params,?BU:strbin("field=master_dbstrings|Invalid 'master_dbstring'. Expected list of posgtresql connection strings. Should contain: host, port, login, pwd, database. Note, system would create new database for subdomain if connection string defines database 'postgres' or the master database of current(parent) domain.",[])}};
                false ->
                    check_2b(Domain,Operation,Item)
            end end.

%% check same database and different host:port for all samples of master db instance
check_2b(Domain,Operation,Item) ->
    DbParamsList = get_item_db_params(Item),
    F = fun(_,{error,_}=Acc) -> Acc;
           (Params,{ok,AccHP,LP0,DB0}) ->
                HP = {maps:get(<<"host">>,Params), maps:get(<<"port">>,Params)},
                LP = {maps:get(<<"login">>,Params), maps:get(<<"pwd">>,Params)},
                DB = maps:get(<<"database">>,Params),
                case maps:is_key(HP,AccHP) of
                    true ->
                        {error, {invalid_params, ?BU:strbin("field=master_dbstrings|Invalid 'master_dbstrings'. Expected different 'host:port' for all samples of master instance",[])}};
                    false when DB0==undefined, LP0==undefined -> {ok,AccHP#{HP=>true},LP,DB};
                    false when DB0/=DB ->
                        {error, {invalid_params, ?BU:strbin("field=master_dbstrings|Invalid 'master_dbstrings'. Expected same 'database' for all samples of master instance",[])}};
                    false when LP0/=LP ->
                        {error, {invalid_params, ?BU:strbin("field=master_dbstrings|Invalid 'master_dbstrings'. Expected same 'login:pwd' for all samples of master instance",[])}};
                    false -> {ok,AccHP#{HP=>true},LP0,DB0}
                end end,
    case lists:foldl(F,{ok,#{},undefined,undefined},DbParamsList) of
        {error,_}=Err -> Err;
        {ok,_,_,_} -> check_x(Domain,Operation,Item)
    end.

%% final
check_x(_Domain,_Operation,Item) -> {ok,Item}.

%% -------------------------------------
%% Check domain complex
%% -------------------------------------

%% unique names
validate_all_domains(Domain, Item) ->
    Id = maps:get(<<"id">>,Item),
    Name = maps:get(<<"name">>,Item),
    case ?DMS_CACHE:read_cache(Domain,?DomainsCN,#{},auto) of
        {error,_}=Err ->
            ?LOG('$error',"~ts. '~ts' validation subdomain '~ts' error: ~120tp",[?APP, Domain, Name, Err]),
            Err;
        {ok,DomainItems,_} ->
            OtherDomains = lists:filter(fun(DomainItem) -> maps:get(<<"id">>,DomainItem) /= Id end, DomainItems),
            OtherNames = lists:map(fun(DomainItem) -> maps:get(<<"name">>,DomainItem) end, OtherDomains),
            case lists:member(Name,OtherNames) of
                true -> {error,{invalid_params,<<"field=name|Name already exists">>}};
                false -> ok
            end
    end.

%% -------------------------------------
%% Check domain complex
%% -------------------------------------

%%
ensure_master_db(Domain,Item) ->
    ItemDbParams = get_item_db_params(Item),
    ParentDbParams = get_parent_db_params(Domain),
    case ?MasterDb:check_connections(format_dbparams(ItemDbParams)) of
        {error,_}=Err -> Err;
        ok ->
            case modify_database(ItemDbParams,ParentDbParams,Item) of
                {error,_}=Err2 -> Err2;
                false -> {ok,Item};
                {true,NewDbParams} ->
                    {ok,Item#{<<"master_dbstrings">> => [build_connstring(P) || P <- NewDbParams]}}
            end end.

%% @private
get_item_db_params(Item) -> get_db_params(maps:get(<<"master_dbstrings">>,Item)).
%% @private
get_db_params(DbStrings) when is_list(DbStrings) ->
    lists:map(fun(DbString) ->
                   String = ?BU:to_list(DbString),
                   maps:from_list([{K,extract(K,String,undefined)} || K <- connstring_keys()])
              end, DbStrings).

%% @private
get_parent_db_params(Domain) ->
    ReadOpts = #{<<"filter">> => [<<"&&">>,[<<"==">>,[<<"property">>,<<"instance">>],?MasterStorageInstance],[<<"==">>,[<<"property">>,<<"type">>],?MasterStorageType]]},
    case ?DMS_CACHE:read_cache(Domain,?StoragesCN,ReadOpts,auto) of
        {error,_}=Err ->
            ?LOG('$error',"~ts. '~ts' validation subdomain '~ts' error reading master storages: ~120tp",[?APP, Domain, Err]),
            Err;
        {ok,[],_} ->
            ?LOG('$error',"~ts. '~ts' validation subdomain '~ts' error: no master storages found",[?APP, Domain]),
            {error,{internal_error,<<"Master storages not found in domain">>}};
        {ok,StorageItems,_} ->
            lists:map(fun(StorageItem) -> maps:get(<<"params">>,StorageItem) end, StorageItems)
    end.

%% @private
modify_database([First|_]=ItemDbParams, ParentDbParams, Item) ->
    ParentDbNames = lists:map(fun(P) -> maps:get(<<"database">>,P) end, ParentDbParams),
    case maps:get(<<"database">>,First) of
        <<"postgres">> -> do_modify_database(ItemDbParams, Item);
        X ->
            case lists:member(X,ParentDbNames) of
                true -> do_modify_database(ItemDbParams, Item);
                false -> false
            end end.

do_modify_database([First|_]=ItemDbParams, Item) ->
    NewDbName = ?DomainDbName(maps:get(<<"name">>,Item)),
    OwnerLogin = maps:get(<<"login">>,First),
    case ?MasterDb:ensure_db(format_dbparams(ItemDbParams), NewDbName, OwnerLogin) of
        {error,_}=Err -> Err;
        ok -> {true, lists:map(fun(P) -> P#{<<"database">> => NewDbName} end, ItemDbParams)}
    end.

%% --------------------------------------
%% Privates
%% --------------------------------------

%% @private
connstring_keys() -> [<<"host">>, <<"port">>, <<"login">>, <<"pwd">>, <<"database">>].

%% @private
extract(Key,String,Default) ->
    case re:run(String,?BU:str("~ts:([^,]*)",[Key])) of
        nomatch -> Default;
        {match,[{_,_},{From,Len}]} -> ?BU:to_binary(string:substr(String,From+1,Len))
    end.

%% @private
format_dbparams(DbParamsList) when is_list(DbParamsList) ->
    lists:map(fun(P) -> format_dbparams(P) end, DbParamsList);
format_dbparams(DbParams) when is_map(DbParams) ->
    #{host => ?BU:to_list(maps:get(<<"host">>,DbParams)),
      port => ?BU:to_int(maps:get(<<"port">>,DbParams,5432)),
      login => ?BU:to_list(maps:get(<<"login">>,DbParams)),
      pwd => ?BU:to_list(maps:get(<<"pwd">>,DbParams)),
      database => ?BU:to_list(maps:get(<<"database">>,DbParams))}.

%% @private
build_connstring(DbParams) when is_map(DbParams) ->
    build_connstring(maps:to_list(DbParams));
build_connstring([{_,_}|_]=DbParams) ->
    ?BU:join_binary(lists:filtermap(fun({_,undefined}) -> false;
                                       ({K,V}) -> {true, ?BU:strbin("~ts:~ts",[K,?BU:to_binary(V)])}
                                    end, DbParams), <<",">>).
