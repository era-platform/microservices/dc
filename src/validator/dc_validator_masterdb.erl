%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.06.2021
%%% @doc Routines of pg

-module(dc_validator_masterdb).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_connections/1,
         ensure_db/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include_lib("epgsql/include/epgsql.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------
%% Connects to db for check
%% ----------------------------
check_connections(DbParamsList) ->
    case ?PgConnectionPool:get_connection(DbParamsList,5000) of
        {ok,Conn} ->
            ok = ?PgConnectionPool:release_connection(DbParamsList,Conn);
        {error,timeout} -> {error,{internal_error,<<"Db connection failed by timeout">>}};
        {error,_}=Err -> try_transform_err(Err)
    end.

%% ----------------------------
%% Connects to db and ensure database (create if not exists)
%% ----------------------------
ensure_db(DbParamsList, DbName, OwnerLogin) ->
    case ?PgConnectionPool:get_connection(DbParamsList,5000) of
        {ok,Conn} ->
            do_ensure_db(Conn,DbName,OwnerLogin),
            ok = ?PgConnectionPool:release_connection(DbParamsList,Conn);
        {error,timeout} -> {error,{internal_error,<<"Db connection failed by timeout">>}};
        {error,_}=Err -> try_transform_err(Err)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
do_ensure_db(Conn,DbName,OwnerLogin) ->
    Sql = sql_ensure_db(DbName,OwnerLogin),
    case catch ?PgDBA:query_db_tout(Conn,Sql,10000) of
        ok -> ok;
        {ok,_Cnt} -> ok;
        {ok,_,_} -> ok;
        {error,_}=Err ->
            ?LOG('$error',"Create database '~ts' got error: ~120tp",[DbName,Err]),
            try_transform_err(Err);
        {'EXIT',Reason} ->
            ?LOG('$error',"Create database '~ts' crashed: ~120tp",[DbName,Reason]),
            {error,{internal_error,<<"Create database error">>}}
    end.

%% @private
sql_ensure_db(DbName,OwnerLogin) ->
    ?BU:strbin("
CREATE DATABASE \"~ts\"
WITH TEMPLATE = template0
ENCODING='UTF8'
OWNER=\"~ts\"
LC_COLLATE = 'C'
LC_CTYPE = 'C'
CONNECTION LIMIT=-1;", [DbName,OwnerLogin]).

%% @private
try_transform_err({error,Reason}=Err) when is_binary(Reason) -> Err;
try_transform_err({error,{_,Reason}}=Err) when is_binary(Reason); is_atom(Reason) -> Err;
try_transform_err({error,#error{severity=Sev,code=Code,codename=CodeN,message=Msg,extra=Extra}}) ->
    JSON = #{severity => Sev,
             code => Code,
             codename => CodeN,
             message => Msg,
             extra => case Extra of _ when is_list(Extra) -> ?BU:json_to_map(Extra); _ when is_map(Extra) -> Extra end},
    {error,jsx:encode(JSON)};
try_transform_err({error,Reason}) -> {error,term_to_binary(Reason)}.